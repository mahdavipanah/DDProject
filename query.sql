-- Database Design Course Project
--
-- Hamidreza Mahdavipanah
-- Pegah Hajian
-- Narges Heydarzadeh

-- tedade parvazhaee k dar yek sal be yek maghsade moshakhas miravand
SELECT COUNT(*) FROM fly
WHERE EXTRACT(YEAR FROM time_stamp) = <year> AND
      destination = <dest>;

-- tedade pilot haee ke faghat be yek maghsad parvaz darand
WITH F AS (SELECT pilot_code, COUNT(DISTINCT destination) FROM fly
	   GROUP BY pilot_code)
SELECT COUNT(*) FROM F
WHERE count = 0;

-- saate parvazhaye yek khalabane moshakhas
SELECT SUM(duration)/60 FROM fly
WHERE pilot_code = <code>;

-- shomare tamas mosaferhayee k ticket VIP be destination moshakhas darand
WITH P AS (SELECT code AS passenger_code, phone_number FROM passenger),
     T AS (SELECT * FROM ticket
	   NATURAL JOIN P)
SELECT DISTINCT phone_number FROM T
NATURAL JOIN fly
WHERE  class = 'VIP' AND
       destination = <dest>;

-- name khadamei k dar ghesmate Flight engineer kar mikonand
SELECT first_name, last_name FROM crew
WHERE duty = 'Flight engineer';

-- majmooe vazn havapeimahaye motealegh be airline x
SELECT SUM(weight) FROM aircraft WHERE airline_code = <code>;

-- zarfiate havapeimahaye k bishtar az x sokhtgiri karde and
SELECT DISTINCT accommodation FROM aircraft
NATURAL JOIN refuel
GROUP BY accommodation
HAVING SUM(amount) > <amount>;

-- name khalabanhaee k ta be hal parvazeshan takhir nadashte ast
WITH T AS (SELECT pilot_code, SUM(delay) FROM fly
	   GROUP BY pilot_code)
SELECT first_name, last_name FROM T
INNER JOIN pilot ON pilot.code = T.pilot_code
WHERE sum = 0;

-- shomare havapeimahayee k bishtar az x sat majmooe takhirhaye anha bashad
SELECT aircraft_code FROM fly
GROUP BY aircraft_code
HAVING SUM(delay) > <delay> * 60;

-- kodam havapeima be nesbate masrafe sookht va tedade mosafer kam bazdeh tarin boode?
WITH R AS (SELECT aircraft_code, SUM(amount) FROM refuel
	   GROUP BY aircraft_code),
     P AS (SELECT aircraft_code, COUNT(*) FROM ticket
	   GROUP BY aircraft_code),
     T AS (SELECT aircraft_code, sum / count AS proportion FROM R
	   NATURAL JOIN P)
SELECT aircraft_code FROM T
ORDER BY proportion DESC
LIMIT 1;

-- 10 shomare sandali ba bishtarin tedade kharid belit
WITH T AS
	(SELECT seat_number, COUNT(*) FROM ticket
	 GROUP BY seat_number
	 ORDER BY count DESC)
SELECT seat_number FROM T
LIMIT 10;

-- 3 maghsade geran (kodam 3 maghsad be tore motevasset gerantarin ticket ra dashte and?)
WITH T AS
	(SELECT destination, AVG(price) FROM fly NATURAL JOIN ticket
	 GROUP BY destination
	 ORDER BY avg DESC )
SELECT destination FROM T
LIMIT 3;

-- pilot ba code <code> be kodam destination bishtar safar karde ast?
WITH T AS (SELECT destination, count(*) FROM fly
	       WHERE pilot_code = <code>
           GROUP BY destination)
SELECT destination from T
ORDER BY count DESC
LIMIT 1;

-- kodam airline bishtarin mosafer ra jabeja karde ast?
WITH T AS (SELECT code AS aircraft_code, airline_code FROM aircraft),
     S AS (SELECT airline_code, count(*) FROM ticket NATURAL JOIN T
	       GROUP BY airline_code)
SELECT airline_code FROM S
ORDER BY count DESC
LIMIT 1;

-- kodam source bishtarin fly ra dar mahe ghabl dashte ast?
WITH T AS
	(SELECT source, count(*) FROM fly
	 WHERE EXTRACT(YEAR FROM time_stamp) = EXTRACT(YEAR FROM CURRENT_TIMESTAMP) AND
	 EXTRACT(MONTH FROM time_stamp) = EXTRACT(MONTH FROM CURRENT_TIMESTAMP) - 1
	 GROUP BY source)
SELECT source FROM T
ORDER BY count DESC
LIMIT 1;

-- name mosaferhaee k ba khalaban x be maghsade y parvaz dashte and
WITH T AS (SELECT * FROM fly NATURAL JOIN ticket),
     S AS (SELECT * FROM T NATURAL JOIN  (SELECT code as passenger_code, first_name, last_name from passenger) AS P)
SELECT first_name, last_name FROM S
WHERE pilot_code = <code> AND
      destination = <dest>;

-- tedad parvazhaee k karmandi ba code <code> dar an hozoor nadashte ast
SELECT count(*) FROM crew_fly
WHERE crew_code <> <code>

-- name 10 mosaferi ke bishtarin hazine ra baraye ticket pardakhte and (be tore kol)
WITH P AS (SELECT code as passenger_code, first_name, last_name from passenger),
     T AS (SELECT passenger_code, SUM(price) FROM ticket
	       GROUP BY passenger_code)
SELECT first_name, last_name FROM T
NATURAL JOIN P
ORDER BY sum DESC
LIMIT 10;

-- parvazi ke az mabdae moshakhas kamtarin takhir ra dashte ast
SELECT aircraft_code, time_stamp FROM fly
WHERE source = <source>
ORDER BY delay ASC
LIMIT 1;

-- dar kodam zaman kamtarin parvaze hamzaman anjam shode ast?
WITH F AS (SELECT time_stamp, count(*) FROM fly
	       GROUP BY time_stamp)
SELECT time_stamp FROM F
ORDER BY count ASC
LIMIT 1;

-- saate parvazi pilot ba code khalabni x
SELECT SUM(duration) FROM fly
WHERE pilot_code = <code>;

-- kodam aircraft baraye khate havee x bishtar hazine fuel ra dashte ast?
WITH T AS (SELECT aircraft_code, SUM(amount) FROM (SELECT airline_code, code as aircraft_code FROM aircraft) AS A
	   NATURAL JOIN refuel
	   WHERE airline_code = <code>
	   GROUP BY aircraft_code)
SELECT aircraft_code FROM T
ORDER BY sum DESC
LIMIT 1;

-- kodam khate havaee bishtar delay ra dar sale akhir dashte ast?
WITH A AS (SELECT airline_code, code as aircraft_code FROM aircraft),
     F AS (SELECT airline_code, SUM(delay) FROM fly
	   NATURAL JOIN A
	   WHERE EXTRACT(YEAR FROM time_stamp) = EXTRACT(YEAR FROM CURRENT_TIMESTAMP)
	   GROUP BY airline_code)
SELECT airline_code FROM F
ORDER BY sum DESC
LIMIT 1;

-- daramadza tarin aircraft baraye airline x kodam boode ast?
WITH A AS (SELECT airline_code, code as aircraft_code FROM aircraft),
     F AS (SELECT aircraft_code, SUM(price) FROM (SELECT * FROM ticket NATURAL JOIN A) as T
	   WHERE T.airline_code = <code>
	   GROUP BY aircraft_code)
SELECT aircraft_code FROM F
ORDER BY sum DESC
LIMIT 1;

-- farsoode tarin aircraft dar airline x bar asase saate parvaz kodam ast?
WITH A AS (SELECT airline_code, code as aircraft_code FROM aircraft),
     F AS (SELECT aircraft_code, SUM(duration) FROM (SELECT * FROM fly NATURAL JOIN A) as T
	   WHERE T.airline_code = <code>
	   GROUP BY aircraft_code)
SELECT aircraft_code FROM F
ORDER BY sum DESC
LIMIT 1;

-- bishtarin mosafer ra kodam ariline jabeja karde ast?
WITH A AS (SELECT airline_code, code as aircraft_code FROM aircraft),
     F AS (SELECT airline_code, count(*) FROM (SELECT * FROM ticket NATURAL JOIN A) as T
	   GROUP BY airline_code)
SELECT airline_code FROM F
ORDER BY count DESC
LIMIT 1;

-- har mosafer be tore motevasset che mizan sookht masraf karde?
WITH R AS (SELECT SUM(amount) FROM refuel),
     P AS (SELECT COUNT(*) FROM passenger)
SELECT sum / count FROM R, P;

-- kodam duty az khadame (crew) bishtarin saate parvazi ra dashte?
WITH F AS (SELECT time_stamp, aircraft_code, duration FROM fly),
     C AS (SELECT code AS crew_code, duty FROM crew),
     CF AS (SELECT duty, SUM(duration) FROM C
	   NATURAL JOIN F
	   GROUP BY duty)
SELECT duty FROM CF
ORDER BY sum DESC
LIMIT 1;

-- kodam class parvaz az hame grantar boode?
WITH T AS (SELECT class, SUM(price) FROM ticket
	   GROUP BY class)
SELECT class FROM T
ORDER BY sum DESC
LIMIT 1;

-- baraye har khate havaee kodam mosaferha bishatarin parvaz ra ba an dashte and?
WITH A AS (SELECT code AS aircraft_code, airline_code FROM aircraft),
     T AS (SELECT airline_code, passenger_code, COUNT(*) FROM ticket
	   NATURAL JOIN A
	   GROUP BY airline_code, passenger_code),
     M AS (SELECT airline_code, MAX(count) FROM T
	   GROUP BY airline_code)
SELECT airline_code, passenger_code FROM T
WHERE count = (SELECT count from M where T.airline_code = M.airline_code);
