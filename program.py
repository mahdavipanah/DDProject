# Database Design Course Project
#
# Hamidreza Mahdavipanah
# Pegah Hajian
# Narges Heydarzadeh

import psycopg2

try:
    conn = psycopg2.connect(database="ddproject", user="ddproject", password="123456",host='127.0.0.1')
except:
    print "Failed to connect to database."
    exit()
cur = conn.cursor()

def q1():
    year = raw_input("Enter a year: ")
    dest = raw_input("Enter a destination: ")
    query = '''
    -- tedade parvazhaee k dar yek sal be yek maghsade moshakhas miravand
    SELECT COUNT(*) FROM fly
    WHERE EXTRACT(YEAR FROM time_stamp) = %s AND
          destination = %s;
    '''
    try:
        cur.execute(query, (year, dest))
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q2():
    query = '''
    -- tedade pilot haee ke faghat be yek maghsad parvaz darand
    WITH F AS (SELECT pilot_code, COUNT(DISTINCT destination) FROM fly
    	   GROUP BY pilot_code)
    SELECT COUNT(*) FROM F
    WHERE count = 0;
    '''
    try:
        cur.execute(query)
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q3():
    pilot_code = raw_input("Enter pilot's code: ")
    query = '''
    -- saate parvazhaye yek khalabane moshakhas
    SELECT SUM(duration)/60 FROM fly
    WHERE pilot_code = %s;
    '''
    try:
        cur.execute(query, [pilot_code])
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q4():
    dest = raw_input("Enter destination: ")
    query = '''
    -- shomare tamas mosaferhayee k ticket VIP be destination moshakhas darand
    WITH P AS (SELECT code AS passenger_code, phone_number FROM passenger),
         T AS (SELECT * FROM ticket
    	   NATURAL JOIN P)
    SELECT DISTINCT phone_number FROM T
    NATURAL JOIN fly
    WHERE  class = 'VIP' AND
           destination =  %s;
    '''
    try:
        cur.execute(query, [dest])
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q5():
    query = '''
    -- name khadamei k dar ghesmate Flight engineer kar mikonand
    SELECT first_name, last_name FROM crew
    WHERE duty = 'Flight engineer';
    '''
    try:
        cur.execute(query)
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q6():
    airline = raw_input("Enter airline's code: ")
    query = '''
    SELECT SUM(weight) FROM aircraft WHERE airline_code = %s;
    '''
    try:
        cur.execute(query, [airline])
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q7():
    amount = raw_input("Enter amount of fuel: ")
    query = '''
    -- zarfiate havapeimahaye k bishtar az x sokhtgiri karde and
    SELECT DISTINCT accommodation FROM aircraft
    NATURAL JOIN refuel
    GROUP BY accommodation
    HAVING SUM(amount) > %s;
    '''
    try:
        cur.execute(query, [amount])
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q8():
    query = '''
    -- name khalabanhaee k ta be hal parvazeshan takhir nadashte ast
    WITH T AS (SELECT pilot_code, SUM(delay) FROM fly
    	   GROUP BY pilot_code)
    SELECT first_name, last_name FROM T
    INNER JOIN pilot ON pilot.code = T.pilot_code
    WHERE sum = 0;
    '''
    try:
        cur.execute(query)
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q9():
    hours = raw_input("Enter hours: ")
    query = '''
    -- shomare havapeimahayee k bishtar az x sat majmooe takhirhaye anha bashad
    SELECT aircraft_code FROM fly
    GROUP BY aircraft_code
    HAVING SUM(delay) > %s * 60;
    '''
    try:
        cur.execute(query, [hours])
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q10():
    query = '''
    WITH R AS (SELECT aircraft_code, SUM(amount) FROM refuel
    	   GROUP BY aircraft_code),
         P AS (SELECT aircraft_code, COUNT(*) FROM ticket
    	   GROUP BY aircraft_code),
         T AS (SELECT aircraft_code, sum / count AS proportion FROM R
    	   NATURAL JOIN P)
    SELECT aircraft_code FROM T
    ORDER BY proportion DESC
    LIMIT 1;
    '''
    try:
        cur.execute(query)
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q11():
    query = '''
    -- 10 shomare sandali ba bishtarin tedade kharid belit
    WITH T AS
    	(SELECT seat_number, COUNT(*) FROM ticket
    	 GROUP BY seat_number
    	 ORDER BY count DESC)
    SELECT seat_number FROM T
    LIMIT 10;
    '''
    try:
        cur.execute(query)
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q12():
    query = '''
    -- 3 maghsade geran (kodam 3 maghsad be tore motevasset gerantarin ticket ra dashte and?)
    WITH T AS
    	(SELECT destination, AVG(price) FROM fly NATURAL JOIN ticket
    	 GROUP BY destination
    	 ORDER BY avg DESC )
    SELECT destination FROM T
    LIMIT 3;
    '''
    try:
        cur.execute(query)
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q13():
    code = raw_input("Enter pilot's code: ")
    query = '''
    -- pilot ba code <code> be kodam destination bishtar safar karde ast?
    WITH T AS (SELECT destination, count(*) FROM fly
    	       WHERE pilot_code = %s
               GROUP BY destination)
    SELECT destination from T
    ORDER BY count DESC
    LIMIT 1;
    '''
    try:
        cur.execute(query, [code])
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q14():
    query = '''
    -- kodam airline bishtarin mosafer ra jabeja karde ast?
    WITH T AS (SELECT code AS aircraft_code, airline_code FROM aircraft),
         S AS (SELECT airline_code, count(*) FROM ticket NATURAL JOIN T
    	       GROUP BY airline_code)
    SELECT airline_code FROM S
    ORDER BY count DESC
    LIMIT 1;
    '''
    try:
        cur.execute(query)
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q15():
    query = '''
    -- kodam source bishtarin fly ra dar mahe ghabl dashte ast?
    WITH T AS
    	(SELECT source, count(*) FROM fly
    	 WHERE EXTRACT(YEAR FROM time_stamp) = EXTRACT(YEAR FROM CURRENT_TIMESTAMP) AND
    	 EXTRACT(MONTH FROM time_stamp) = EXTRACT(MONTH FROM CURRENT_TIMESTAMP) - 1
    	 GROUP BY source)
    SELECT source FROM T
    ORDER BY count DESC
    LIMIT 1;
    '''
    try:
        cur.execute(query)
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q16():
    code = raw_input("Enter pilot's code: ")
    dest = raw_input("Enter destination: ")
    query = '''
    -- name mosaferhaee k ba khalaban x be maghsade y parvaz dashte and
    WITH T AS (SELECT * FROM fly NATURAL JOIN ticket),
         S AS (SELECT * FROM T NATURAL JOIN  (SELECT code as passenger_code, first_name, last_name from passenger) AS P)
    SELECT first_name, last_name FROM S
    WHERE pilot_code = %s AND
          destination = %s;
    '''
    try:
        cur.execute(query, [code, dest])
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q17():
    code = raw_input("Enter pilot's code: ")
    query = '''
    -- tedad parvazhaee k karmandi ba code <code> dar an hozoor nadashte ast
    SELECT count(*) FROM crew_fly
    WHERE crew_code <> %s
    '''
    try:
        cur.execute(query, [code])
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q18():
    query = '''
    -- name 10 mosaferi ke bishtarin hazine ra baraye ticket pardakhte and (be tore kol)
    WITH P AS (SELECT code as passenger_code, first_name, last_name from passenger),
         T AS (SELECT passenger_code, SUM(price) FROM ticket
    	       GROUP BY passenger_code)
    SELECT first_name, last_name FROM T
    NATURAL JOIN P
    ORDER BY sum DESC
    LIMIT 10;
    '''
    try:
        cur.execute(query)
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q19():
    source = raw_input("Enter source: ")
    query = '''
    SELECT aircraft_code, time_stamp FROM fly
    WHERE source = %s
    ORDER BY delay ASC
    LIMIT 1;
    '''
    try:
        cur.execute(query, [source])
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q20():
    query = '''
    -- dar kodam zaman kamtarin parvaze hamzaman anjam shode ast?
    WITH F AS (SELECT time_stamp, count(*) FROM fly
    	       GROUP BY time_stamp)
    SELECT time_stamp FROM F
    ORDER BY count ASC
    LIMIT 1;
    '''
    try:
        cur.execute(query)
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q21():
    code = raw_input("Enter pilot's code: ")
    query = '''
    -- saate parvazi pilot ba code khalabni x
    SELECT SUM(duration) FROM fly
    WHERE pilot_code = %s;
    '''
    try:
        cur.execute(query, [code])
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q22():
    code = raw_input("Enter airline's code: ")
    query = '''
    -- kodam aircraft baraye khate havee x bishtar hazine fuel ra dashte ast?
    WITH T AS (SELECT aircraft_code, SUM(amount) FROM (SELECT airline_code, code as aircraft_code FROM aircraft) AS A
    	   NATURAL JOIN refuel
    	   WHERE airline_code = %s
    	   GROUP BY aircraft_code)
    SELECT aircraft_code FROM T
    ORDER BY sum DESC
    LIMIT 1;
    '''
    try:
        cur.execute(query, [code])
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q23():
    query = '''
    -- kodam khate havaee bishtar delay ra dar sale akhir dashte ast?
    WITH A AS (SELECT airline_code, code as aircraft_code FROM aircraft),
         F AS (SELECT airline_code, SUM(delay) FROM fly
    	   NATURAL JOIN A
    	   WHERE EXTRACT(YEAR FROM time_stamp) = EXTRACT(YEAR FROM CURRENT_TIMESTAMP)
    	   GROUP BY airline_code)
    SELECT airline_code FROM F
    ORDER BY sum DESC
    LIMIT 1;
    '''
    try:
        cur.execute(query)
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q24():
    code = raw_input("Enter airline's code")
    query = '''
    -- daramadza tarin aircraft baraye airline x kodam boode ast?
    WITH A AS (SELECT airline_code, code as aircraft_code FROM aircraft),
         F AS (SELECT aircraft_code, SUM(price) FROM (SELECT * FROM ticket NATURAL JOIN A) as T
    	   WHERE T.airline_code = <code>
    	   GROUP BY aircraft_code)
    SELECT aircraft_code FROM F
    ORDER BY sum DESC
    LIMIT 1;
    '''
    try:
        cur.execute(query, [code])
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q25():
    code = raw_input("Enter airline's code: ")
    query = '''
    -- farsoode tarin aircraft dar airline x bar asase saate parvaz kodam ast?
    WITH A AS (SELECT airline_code, code as aircraft_code FROM aircraft),
         F AS (SELECT aircraft_code, SUM(duration) FROM (SELECT * FROM fly NATURAL JOIN A) as T
    	   WHERE T.airline_code = %s
    	   GROUP BY aircraft_code)
    SELECT aircraft_code FROM F
    ORDER BY sum DESC
    LIMIT 1;
    '''
    try:
        cur.execute(query, [code])
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q26():
    query = '''
    -- bishtarin mosafer ra kodam ariline jabeja karde ast?
    WITH A AS (SELECT airline_code, code as aircraft_code FROM aircraft),
         F AS (SELECT airline_code, count(*) FROM (SELECT * FROM ticket NATURAL JOIN A) as T
    	   GROUP BY airline_code)
    SELECT airline_code FROM F
    ORDER BY count DESC
    LIMIT 1;
    '''
    try:
        cur.execute(query)
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q27():
    query = '''
    -- har mosafer be tore motevasset che mizan sookht masraf karde?
    WITH R AS (SELECT SUM(amount) FROM refuel),
         P AS (SELECT COUNT(*) FROM passenger)
    SELECT sum / count FROM R, P;
    '''
    try:
        cur.execute(query)
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q28():
    query = '''
    -- kodam duty az khadame (crew) bishtarin saate parvazi ra dashte?
    WITH F AS (SELECT time_stamp, aircraft_code, duration FROM fly),
         C AS (SELECT code AS crew_code, duty FROM crew),
         CF AS (SELECT duty, SUM(duration) FROM C
    	   NATURAL JOIN F
    	   GROUP BY duty)
    SELECT duty FROM CF
    ORDER BY sum DESC
    LIMIT 1;
    '''
    try:
        cur.execute(query)
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q29():
    query = '''
    -- kodam class parvaz az hame grantar boode?
    WITH T AS (SELECT class, SUM(price) FROM ticket
    	   GROUP BY class)
    SELECT class FROM T
    ORDER BY sum DESC
    LIMIT 1;
    '''
    try:
        cur.execute(query)
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

def q30():
    query = '''
    -- baraye har khate havaee kodam mosaferha bishatarin parvaz ra ba an dashte and?
    WITH A AS (SELECT code AS aircraft_code, airline_code FROM aircraft),
         T AS (SELECT airline_code, passenger_code, COUNT(*) FROM ticket
    	   NATURAL JOIN A
    	   GROUP BY airline_code, passenger_code),
         M AS (SELECT airline_code, MAX(count) FROM T
    	   GROUP BY airline_code)
    SELECT airline_code, passenger_code FROM T
    WHERE count = (SELECT count from M where T.airline_code = M.airline_code);
    '''
    try:
        cur.execute(query)
        for a in cur.fetchall():
            print(a)
    except psycopg2.Error as e:
        print
        print e.pgerror

queries = [
    {
        'title': 'Number of flies to a specific destination in a year',
        'func' : q1
    },
    {
        'title': 'Number of pilots who had only flied to one destination',
        'func': q2
    },
    {
        'title': 'Fly hours of a specific pilot',
        'func': q3
    },
    {
        'title': 'Phone numbers of passengers who has VIP fly to a specific destination',
        'func': q4
    },
    {
        'title': 'Name of crews who work as flight engineer',
        'func': q5
    },
    {
        'title': 'Total weight of aircrafts belonging to a specific airline',
        'func': q6
    },
    {
        'title': 'Accommodation of aircrafts who has refuled more than a specific amount',
        'func': q7
    },
    {
        'title': 'Name of pilots who have not had any delayed flight',
        'func': q8
    },
    {
        'title': 'Aircrafts which have had totally more than a specific hours of delayed',
        'func': q9
    },
    {
        'title': 'The aircraft with lowest efficiency (fuel amount/number of passengers)',
        'func': q10
    },
    {
        'title': 'Ten best selling seat numbers',
        'func': q11
    },
    {
        'title': 'Three most expensive destinations',
        'func': q12
    },
    {
        'title': 'A specific pilot\'s most visited destination',
        'func': q13
    },
    {
        'title': 'Which airline has transfered more passengers',
        'func': q14
    },
    {
        'title': 'Which source city had more flights last month',
        'func': q15
    },
    {
        'title': 'Name of passengers who had a flight with a specific pilot to a specific destination',
        'func': q16
    },
    {
        'title': 'Number of flies that a specific crew has not been in',
        'func': q17
    },
    {
        'title': 'Ten passengers who have spent more money for flying',
        'func': q18
    },
    {
        'title': 'Flight with lowest delay from a specific source',
        'func': q19
    },
    {
        'title': 'Time with lowest number of flies',
        'func': q20
    },
    {
        'title': 'Fly hourse of a specific pilot',
        'func': q21
    },
    {
        'title': 'Which aircraft had burned more fuel for a specific airline',
        'func': q22
    },
    {
        'title': 'Which airline had more delayed flights in the last year',
        'func': q23
    },
    {
        'title': 'What is the most benefitial aircraft for a specific airline',
        'func': q24
    },
    {
        'title': "Which aircraft is the oldest in a specific airline",
        'func': q25
    },
    {
        'title': "Which airline has transfered more passengers",
        'func': q26
    },
    {
        'title': "How much fuel has every passenger burned averagly",
        'func': q27
    },
    {
        'title': "Which duty has the highest fly time",
        'func': q28
    },
    {
        'title': "Which flight class is the most expensive one",
        'func': q29
    },
    {
        'title': "Determine the passenger with highest number of flights for every airline",
        'func': q30
    }
]

select = -1
while select < 1 or select > len(queries) + 1:
    print("Select one of these queries:")
    for i in range(0, len(queries)):
        print '    {} - {}'.format(i + 1, queries[i]['title'])
    select = int(raw_input(":"))
    print("")
    queries[select - 1]['func']()

cur.close()
conn.close()
