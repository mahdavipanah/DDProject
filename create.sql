-- Database Design Course Project
--
-- Hamidreza Mahdavipanah
-- Pegah Hajian
-- Narges Heydarzadeh

CREATE TABLE IF NOT EXISTS airline (
    code INTEGER PRIMARY KEY,
    name TEXT NOT NULL UNIQUE,
    phone TEXT NOT NULL,
    office_address TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS aircraft (
    code INTEGER PRIMARY KEY,
    airline_code INTEGER REFERENCES airline(code),
    model TEXT NOT NULL,
    weight REAL NOT NULL,
    accommodation INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS refuel (
    amount INTEGER NOT NULL CHECK (amount > 0),
    time_stamp TIMESTAMP,
    aircraft_code INTEGER REFERENCES aircraft(code),
    PRIMARY KEY(time_stamp, aircraft_code)
);

CREATE TABLE IF NOT EXISTS pilot (
    code INTEGER PRIMARY KEY,
    first_name TEXT NOT NULL,
    last_name TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS fly (
    aircraft_code INTEGER REFERENCES aircraft(code),
    time_stamp TIMESTAMP,
    pilot_code INTEGER REFERENCES pilot(code),
    source TEXT NOT NULL,
    destination TEXT NOT NULL,
    duration INTEGER NOT NULL,
    delay INTEGER NOT NULL,
    PRIMARY KEY(aircraft_code, time_stamp)
);

CREATE TABLE IF NOT EXISTS duty (
    title TEXT PRIMARY KEY,
    rank SMALLINT NOT NULL
);

CREATE TABLE IF NOT EXISTS crew (
    code INTEGER PRIMARY KEY,
    duty TEXT REFERENCES duty(title),
    first_name TEXT NOT NULL,
    last_name TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS crew_fly (
    crew_code INTEGER REFERENCES crew(code),
    aircraft_code INT,
    time_stamp TIMESTAMP,
    FOREIGN KEY(aircraft_code, time_stamp) REFERENCES fly(aircraft_code, time_stamp),
    PRIMARY KEY(crew_code, aircraft_code, time_stamp)
);

CREATE TABLE IF NOT EXISTS passenger (
    code INTEGER PRIMARY KEY,
    first_name TEXT NOT NULL,
    last_name TEXT NOT NULL,
    phone_number TEXT
);

CREATE TABLE IF NOT EXISTS travel_class (
    name TEXT PRIMARY KEY,
    priority SMALLINT NOT NULL
);

CREATE TABLE IF NOT EXISTS ticket (
    passenger_code INTEGER REFERENCES passenger(code),
    aircraft_code INTEGER,
    time_stamp TIMESTAMP,
    seat_number INTEGER NOT NULL,
    class TEXT REFERENCES travel_class(name),
    price BIGINT NOT NULL,
    FOREIGN KEY(aircraft_code, time_stamp) REFERENCES fly(aircraft_code, time_stamp),
    PRIMARY KEY(aircraft_code, time_stamp, passenger_code, seat_number)
);
